function mvs = m_v_speed(s)
    x = s(:, 1);
    y = s(:, 2);
    
    pts = [x(2:end) y(2:end) x(1:end-1) y(1:end-1)];
    
    mvs = sum(abs((pts(:, 2) - pts(:, 4)))) ./ totalTime(s);
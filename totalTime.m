function tt = totalTime(s)

ts = s(:, 3);

tt = abs(max(ts) - min(ts));
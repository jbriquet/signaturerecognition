function main(InputFile, OutputFile)

% USE EXAMPLE: main('input/input1' 'output1');

load svmStruct;

fileArray = textread(InputFile, '%s');
arraySize = size(fileArray);

maxI = arraySize(1)/2 - 1;
fid = fopen(OutputFile, 'w');

for i=0:maxI
    
    signRef = parse(fileArray{i*2+1});
    signComp = parse(fileArray{i*2+2});
    bla = signRef;
    blu = signComp;
    %
    % REFERENCE SIGNATURE NORMALISATION
    %
    % Handling Rotation
    rotRef = signRotate(signRef);
    signRef(:, 1) = rotRef(:, 1);
    signRef(:, 2) = rotRef(:, 2);
    
    % Handling translation to Center
    transRef = translateToCenter(signRef);
    signRef(:, 1) = transRef(:, 1);
    signRef(:, 2) = transRef(:, 2);
    
    %
    % COMPARED SIGNATURE NORMALISATION
    %
    % Handling Rotation
    rotComp = signRotate(signComp);
    signComp(:, 1) = rotComp(:, 1);
    signComp(:, 2) = rotComp(:, 2);
    
    % Handling translation to Center
    transComp = translateToCenter(signComp);
    signComp(:, 1) = transComp(:, 1);
    signComp(:, 2) = transComp(:, 2);
    
    % Perfom DTW between signatures
    dtwV = zeros(1,7);
    
    for k=1:7
        dtwV(1, k) = dtw(signRef(:, k), signComp(:, k));
    end
    
    % Prediction
    pred = svmclassify(svmStruct, dtwV);
    samplescaleshift = bsxfun(@plus,dtwV, svmStruct.ScaleData.shift);
    dtwV = bsxfun(@times,samplescaleshift,svmStruct.ScaleData.scaleFactor);
    sv = svmStruct.SupportVectors;
    alphaHat = svmStruct.Alpha;
    bias = svmStruct.Bias;
    kfun = svmStruct.KernelFunction;
    kfunargs = svmStruct.KernelFunctionArgs;
    f = kfun(sv,dtwV,kfunargs{:})'*alphaHat(:) + bias;
    
    finalDist = f;

%    if (pred == 'T')
%        finalDist = '1';
%    else
%        finalDist = '-1';
%    end

    decision = pred;
    
    if fid ~= -1
        str = [fileArray{i*2+1}, ' ', fileArray{i*2+2}, ' ', num2str(finalDist), ' ', decision];
        fprintf(fid,'%s\r\n',str);
    end
    
end

if fid ~= -1
    fclose(fid);
end
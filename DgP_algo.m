function recList = DgP_algo(PtsList, eps)
    dmax = -inf;
    idx = 0;

    [n, p] = size(PtsList);
    
    if n <= 2
        recList = PtsList;
        return;
    end
    
    for i = 2:n-1
        d = dist(PtsList(i, :), PtsList([1, n], :));
        if d > dmax
            dmax = d;
            idx = i;
        end
    end
    
    if dmax > eps
        recList1 = DgP_algo(PtsList(1:idx, :), eps);
        recList2 = DgP_algo(PtsList(idx:n, :), eps);
        
        recList = [recList1; recList2(2:end, :)];
    else
        recList = PtsList([1,n], :);
    end
    
    function d = dist(pt, line)
       Xa = line(1, 1);
       Xb = line(1, 2);
       Ya = line(2, 1);
       Yb = line(2, 2);
       
       dline = sqrt((Ya - Yb).^2 + (Xa - Xb).^2);
       
       if dline > eps
           d = abs(det([1 1 1;pt(1) Xa Xb;pt(2) Ya Yb]))/dline;
       else
           d = sqrt((pt(2) - Ya).^2 + (pt(1) - Xa).^2);
       end
    
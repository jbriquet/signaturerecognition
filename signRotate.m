function rot=signRotate(A)

%plot(A(:,1), A(:,2))
%hold on;
sol = polyfit(A(:,1), A(:,2), 1);
b = sol(2);
a = sol(1);
%plot (a*A(:,1) + b)

theta = -atan(a);

rot(:,1) = (cos(theta)*A(:,1))+((-sin(theta))*(A(:,2)-b));
rot(:,2) = ((sin(theta)*A(:,1))+(cos(theta)*(A(:,2)-b)))+b;
%hold on
%sol2 = polyfit(rot(:,1), rot(:,2), 1)
%b = sol(2)
%a = sol(1)
%plot(a*1:100 + b)

%res = rot * tmp

%plot(rot(:,1), rot(:,2), '-r')


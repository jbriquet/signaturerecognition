function M=translateToCenter(M)

cx = mean(M(:,1));
cy = mean(M(:,2));


M(:,1) = M(:,1) - cx;
M(:,2) = M(:,2) - cy;


%plot(M(:,1), M(:,2))
%hold on
%plot(cx, cy, '-r')

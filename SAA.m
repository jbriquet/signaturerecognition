function saa = SAA(s)
    x = s(:, 1);
    y = s(:, 2);
    
    saa = sum(atan(diff(y) ./ diff(x)));
function dv = DV_algo(s)

    d = inf;
    threshold = 0.7; %a set
    eps = 10; %a set
    res = inf;
    
    while d > threshold
        Xe = DgP_algo(s(:, 1:2), eps);
        N = length(Xe);
        tmp = 1 - log(N / eps) / log(eps);
        d = abs(res - tmp);
        eps = eps / 2;
        res = tmp;
    end
    
    dv = res;
function ms = m_speed(s)
    x = s(:, 1);
    y = s(:, 2);
    
    pts = [x(2:end) y(2:end) x(1:end-1) y(1:end-1)];
    
    ms = sum(sqrt((pts(:, 1) - pts(:, 4)).^2 - (pts(:, 1) - pts(:, 3)).^2)) / totalTime(s);
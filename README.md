# README #


### What is this repository for? ###

* This repertory contains several MATLAB scripts used for signature recognition.
* Version 1.0

### How do I use this program? ###

* This program (compSign) is only compatible with **Linux** (sh script)
* Matlab need to be installed first in order to launch the program
* The program must be launched as the following: **./compSign <input file> <output file>**
* The input file is a list of TXT file containing signature. Each line correspond to the two signature that will be compared (see example in input dir)
* The output file will contain the result as the following: <first signature> <second signature> <signature comparison score (integer)> <decision (T or F)>
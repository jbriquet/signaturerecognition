function [M, group] = extract_features(cells)
M = zeros(0, 8);
tmp = zeros(1, 8);
group = zeros(0, 1);

nbok = 20;
for u=1:5
    align = (u-1)*(2*nbok);
%    for i=((u-1)*40)+1:((u-1)*40)+20
    for i = align + 1:align + nbok
        for j=i:align + (2*nbok)
            if i ~= j 
%                 for k=1:7
%                     if k ~= 4
%                         tmp(1, k)  = dtw(cells{i} (:, k), cells{j} (:,
%                         k));
%                     else
%                         tmp(1, k) = nnz(find(cells{i} (:, k) == 1)) >=
%                         nnz(find(cells{i} (:, k) == 0));
%                     end
%                  end

                tmp(1, 1) = dtw(cells{i}, cells{j});
                tmp(1, 2) = abs(m_speed(cells{i}) - m_speed(cells{j}));
                tmp(1, 3) = abs(m_h_speed(cells{i}) - m_h_speed(cells{j}));
                tmp(1, 4) = abs(m_v_speed(cells{i}) - m_v_speed(cells{j}));
                tmp(1, 5) = abs(m_acc(cells{i}) - m_acc(cells{j}));
                tmp(1, 6) = abs(SAA(cells{i}) - SAA(cells{j}));
                tmp(1, 7) = abs(DV_algo(cells{i}) - DV_algo(cells{j}));
                tmp(1, 8) = abs(totalTime(cells{i}) - totalTime(cells{j})); 
                
                M = cat(1, M, tmp);
                if j <= (align+nbok)
                    group = cat(1, group, 'T');
                else
                    group = cat(1, group, 'F');
                end
            end
        end
    end
end

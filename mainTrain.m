function mainTrain(directory)
list = dir(directory);
cells = cell(length(list),1);
%M = zeros(0, 0);
idx = 1;

for i=1:length(list)
    if ~list(i).isdir
        cells{idx} = parse(fullfile(directory, list(i).name));
        rotMat = signRotate(cells{idx});
        cells{idx}(:, 1) = rotMat(:, 1);
        cells{idx}(:, 2) = rotMat(:, 2);
        transMat = translateToCenter(cells{idx});
        cells{idx}(:, 1) = transMat(:, 1);
        cells{idx}(:, 2) = transMat(:, 2);

%        cells{idx} = signRotate(cells{idx});
%        cells{idx} = translateToCenter(cells{idx});
        idx = idx + 1;
    end
end


[M, group] = extract_features(cells);
svmStruct = svmtrain(M, group);
save('svmStruct.mat', 'svmStruct');
%load('svmStruct.mat', 'svmStruct');
%pred = svmclassify(svmStruct, M);
%length(pred)
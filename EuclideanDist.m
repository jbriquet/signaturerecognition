function d=EuclideanDist(vect1, vect2)
d = 0;
[dimX1, dimY1] = size(vect1);
[dimX2, dimY2] = size(vect2);
for i=1:dimY1
    d = d + (vect1(i) - vect2(i)) .^2;
end
d = sqrt(d);

function M=dtw(A,B,w)

td = cputime;
ay=length(A);
by=length(B);

w=Inf;

%w=max(w, abs(ay-by));

DynTimWarp=zeros(ay+1,by+1)+Inf;
DynTimWarp(1,1)=0;

%tEuclid = 0;
%tCost = 0;

for i=1:ay
    for j=max(i-w,1):min(i+w,by)

        %t = cputime;
        %cost=EuclideanDist(A(i), B(j));
        cost = norm(A(i) - B(j));
        %tEuclid = tEuclid + (cputime - t);
        
        %t = cputime;
        DynTimWarp(i+1,j+1)=cost + min([DynTimWarp(i,j+1), DynTimWarp(i+1,j), DynTimWarp(i,j)]);
        %tCost = tCost + (cputime - t);
        %strDTW = ['Comp actual cost: ', num2str(e)]
        
    end
end

%strDTW = ['Euclidean comp: ', num2str(tEuclid)]
%strDTW = ['Cost comp: ', num2str(tCost)]



% Remove useless col/row
%DynTimWarp(1,:) = [];
%DynTimWarp(:,1) = [];


matSize = size(DynTimWarp);

% Return
M=DynTimWarp(matSize(1), matSize(2));

%tf = cputime - td;
%strDTW = ['All DTW comp: ', num2str(tf)]

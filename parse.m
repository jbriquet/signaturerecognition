function M = parse(filename)

fid = fopen(filename, 'r');

nbPoint = fscanf(fid, '%d', 1);
A = zeros(0,0);

while ~feof(fid)
  line = fscanf(fid, '%f %f %f %f %f %f %f', [1 7]);
  A = cat(1, A, line);
end

M = A;
fclose(fid);
